#Class MyPlayer creates player's strategy for Prisoner's dilemma game

#I use Dominant strategy and Pavlov strategy. Dominant strategy finds out whether better payoffs are for COOP or DEF.
#Then it plays accordingly.

#Pavlov strategy returns best mutual response when the opponent does the same. If not, Pavlov strategy returns the opposite
#response until the opponent does so. Then, it starts to return best mutual response again.

#Author: Stanislav Lamos

class MyPlayer:
    """Class MyPlayer combines Pavlov and Dominant strategy"""
    def __init__(self, payoff_matrix, number_of_iterations = None):     #Assigning arguments of the function
        self.number_of_iterations = number_of_iterations
        self.payoff_matrix = payoff_matrix

        #Creating new variables in order to execute my strategy successfuly
        self.pm_dictionary = {}
        self.coop = False
        self.defe = True
        self.opponent_moves_counter = {True: 0, False: 0}
        self.opponent_moves_array = []
        self.my_moves_array = []
        self.counter = 0
        self.playing_column = 1
        self.strategy = "Dominant"

        #Adding values from payoff matrix to dictionary
        self.pm_dictionary[("c", "c")] = (self.payoff_matrix[0][0][0], self.payoff_matrix[0][0][1])
        self.pm_dictionary[("d", "d")] = (self.payoff_matrix[1][1][0], self.payoff_matrix[1][1][1])
        self.pm_dictionary[("c", "d")] = (self.payoff_matrix[0][1][0], self.payoff_matrix[0][1][1])
        self.pm_dictionary[("d", "c")] = (self.payoff_matrix[1][0][0], self.payoff_matrix[1][0][1])

        #Analyzing my payoff from the matrix
        self.my_coop_coop_payoff = self.pm_dictionary[("c", "c")][0]
        self.my_coop_def_payoff = self.pm_dictionary[("c", "d")][0]
        self.my_def_coop_payoff = self.pm_dictionary[("d", "c")][0]
        self.my_def_def_payoff = self.pm_dictionary[("d", "d")][0]

        #Evaluating best mutual response with the opponent based on the matrix
        if self.my_coop_coop_payoff >= self.my_def_def_payoff:
            self.best_mutual_response = self.coop

        else:
            self.best_mutual_response = self.defe

    #Getting my and opponent's last move
    def record_last_moves(self, my_last_move, opponent_last_move):
        self.my_last_move = my_last_move
        self.opponent_last_move = opponent_last_move
        self.my_moves_array.append(self.my_last_move)
        self.opponent_moves_array.append(self.opponent_last_move)
        self.opponent_moves_counter[self.opponent_last_move] += 1

    #Main method which executes my strategy
    def move(self):
        #Declaring methods which are crucial for my strategy
        self.array_shortener()
        self.detect_tit_for_tat()
        self.detect_myself()

        #If you detect Tit for tat or me playing against myself, return best mutual response
        if self.strategy == "Tit for tat" or self.strategy == "Playing against myself":
            return self.best_mutual_response

        #If we played less than three rounds, play dominant strategy (if possible) or play Pavlov strategy
        elif len(self.opponent_moves_array) < 3 and len(self.opponent_moves_array) != 0:
            if self.evaluate_dominant_strategy() == "No":
                return self.pavlov_strategy()

            else:
                return self.evaluate_dominant_strategy()

        #After first three rounds, I can better analyze my oponnent and pick the best tactic
        elif len(self.opponent_moves_array) > 3:

            #If the opponent plays worse possibility against my Dominant strategy three times, I start to play Pavlov strategy
            if self.opponent_moves_array[-3:] == self.weaker_play_in_dominant_strategy() and self.counter != 3:
                self.counter += 1   #Incrementing counter of infavourable moves against my Dominant strategy
                return self.pavlov_strategy()

            #If the counter of infavourable moves against my Dominant strategy is equal to 3, start playing Pavlov strategy
            elif self.counter == 3:
                return self.pavlov_strategy()

            #If the Dominant strategy is not excutable in this matrix, play Pavlov strategy
            elif self.evaluate_dominant_strategy() == "No":
                return self.pavlov_strategy()

            #If the
            elif self.opponent_moves_array[-3:] != self.weaker_play_in_dominant_strategy():
              return self.evaluate_dominant_strategy()

        #First round return best mutual response
        else:
            return self.best_mutual_response

    #Evaluating whether Dominant strategy is executable in given payoff matrix
    def evaluate_dominant_strategy(self):
        #If the payoff on the first row is higher, return False
        if self.my_coop_coop_payoff >= self.my_def_coop_payoff and self.my_coop_def_payoff >= self.my_def_def_payoff:
            self.playing_column = 0
            return self.coop

        #If the payoff on the second row is higher, return True
        elif self.my_coop_coop_payoff < self.my_def_coop_payoff and self.my_coop_def_payoff < self.my_def_def_payoff:
            self.playing_column = 1
            return self.defe

        #If Dominant strategy is not possible to play, return "No"
        else:
            return "No"

    #Shorten the arrays, which stores my and opponents previous moves, in order to reduce memory usage
    def array_shortener(self):
        #If the array is longer than 100 items, delete first 50 items
        if len(self.opponent_moves_array) > 100 or len(self.my_moves_array) > 100:
            del self.opponent_moves_array[0:50]
            del self.my_moves_array[0:50]

    #Finding infavourable opponet's move against my Dominant strategy
    def weaker_play_in_dominant_strategy(self):
        #If the infavourable opponent's move against my strategy is Defect, return Defect
        if self.payoff_matrix[self.playing_column][0][0] >= self.payoff_matrix[self.playing_column][1][0]:
            return [self.defe] * 3

        #Else, the infavourable opponent's move is Cooperation
        else:
            return [self.coop] * 3

    #Executing Pavlov strategy
    def pavlov_strategy(self):
        #If the my last move and opponet's last move are the same, return best mutual response
        if self.opponent_last_move == self.my_last_move:
            return self.best_mutual_response

        #If they are different, return the opposite value of best mutual response
        else:
            return not self.best_mutual_response

    #Detecting Tit for Tat strategy
    def detect_tit_for_tat(self):
        #If my moves and opponents moves are the same after 9 iterations, set the strategy to Pavlov
        if self.my_moves_array[4:9] == self.opponent_moves_array[5:10] and len(self.opponent_moves_array) > 12:
            self.strategy = "Tit for tat"

    #Detecting whether I am playing against myself
    def detect_myself(self):
        #If my moves and opponents are the same after 10 rounds, set the stretgy to "Playing against myself"
        if self.my_moves_array[-10:] == self.opponent_moves_array[-10:] and len(self.opponent_moves_array) > 10:
            self.strategy = "Playing against myself"




