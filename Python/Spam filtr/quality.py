import confmat
import utils
import os



def quality_score(tn, tp, fn, fp):
    ret = (tp + tn) / (tp + tn + (10*fp) + fn)
    return ret

def compute_quality_for_corpus (corpus_dir):
    os.chdir(corpus_dir)
    truth_dict = utils.read_classification_from_file("!truth.txt")
    prediction_dict = utils.read_classification_from_file("!prediction.txt")
    cm = confmat.BinaryConfusionMatrix ("SPAM", "OK")
    cm.compute_from_dicts(truth_dict, prediction_dict)

    return quality_score(cm.tn, cm.tp, cm.fn, cm.fp)
