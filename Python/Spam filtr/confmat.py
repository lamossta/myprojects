class BinaryConfusionMatrix:
    def __init__ (self, pos_tag, neg_tag):
        self.tp = 0
        self.tn = 0
        self.fp = 0
        self.fn = 0
        self.pos_tag = pos_tag
        self.neg_tag = neg_tag

    def as_dict(self):
        return {'tp' : self.tp, 'tn': self.tn, 'fp' : self.fp, 'fn' : self.fn}


    def update(self, truth, prediction):
        if truth not in  (self.pos_tag, self.neg_tag):
            raise ValueError ('Wrong truth value')

        if prediction not in  (self.pos_tag, self.neg_tag):
            raise ValueError ('Wrong prediction value')

        if truth == prediction == self.pos_tag:
            self.tp += 1

        if truth == prediction == self.neg_tag:
            self.tn += 1

        if prediction == self.pos_tag and truth == self.neg_tag:
            self.fp += 1

        if prediction == self.neg_tag and truth == self.pos_tag:
            self.fn += 1

    def compute_from_dicts (self, truth_dict, pred_dict):
        for key in truth_dict:
            self.update (truth_dict[key], pred_dict[key])


