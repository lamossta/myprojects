#MyPLayer class evaluates best move based on Heuristic array
#Heuristic array assings score to every field on board (for example: the corners are the most valuable fields on board)
#Author: Stanislav Lamos

class MyPlayer:
    """Player evaluates best move based on heuristic array"""
    def __init__(self, my_color, opponent_color):
        self.name = "lamossta"
        self.my_color = my_color
        self.opponent_color = opponent_color

        #Array of tuples which represents directions used to iterate through the board and find valid moves
        self.directions = [
            (-1, -1),
            (-1, 0),
            (-1, 1),
            (0, -1),
            (0, 1),
            (1, -1),
            (1, 0),
            (1, 1),
        ]

        #Array which assign score to every field on board. Higher the score is, more valuable the field is for my player
        self.boardHeuristic = [
            [1000, -10, 10, 10, 10, 10, -10, 1000], \
            [-10, -10, 10, 1, 1, 10, -10, -10], \
            [10, 10, 10, 1, 1, 10, 10, 10], \
            [10, 1, 1, 1, 1, 1, 1, 10], \
            [10, 1, 1, 1, 1, 1, 1, 10], \
            [10, 10, 10, 1, 1, 10, 10, 10], \
            [-10, -10, 10, 1, 1, 10, -10, -10], \
            [1000, -10, 10, 10, 10, 10, -10, 1000]]

        self.results = []       #List of all possible moves my player can play in this round
        self.candidates = []    #List of all fields which meet one condition - to have at least one opponent's stone in
                                #its closest surroundings

        self.empties = []       #List of all empty fields on board
        self.empty = -1         #Value of empty field on board

    #Function which returns my move for this round
    def move(self, board):
        return_value = 0
        self.board = board  #Array which represents curremt state of the board

        #Iterating through the entire board to search all empty fields
        for row in range(8):
            for col in range(8):
                #If the field is empty, append its coordinates to self.empties
                if self.board[row][col] == self.empty:
                    self.empties.append((row, col))

        #Calling self.first_try() function
        self.first_try()

        #If lenght of candidates is higher than 0, execute self.possible_moves() function
        if len(self.candidates) > 0:
            self.possible_moves()

            #If the array of results is higher than 0, return best move according to Heuristic array
            if len(self.results) > 0:
                return_value = self.weighted_moves()
            #Else, return None
            else:
                return_value = None

        #If lenght of candidates is not higher than 0, return None
        else:
            return_value = None

        #Clearing all used arrays fater each round
        self.candidates.clear()
        self.results.clear()
        self.empties.clear()

        #Returning my move for current round
        return return_value

    #Function which tests one condition - whether field have at least one opponent's stone in its surroundings
    def first_try(self):
        #Iterate through array of all empty fileds on board
        for i in self.empties:
            firstX = i[0]
            firstY = i[1]

            #Incrementing value of direction only once
            for direction in self.directions:
                firstX += direction[0]
                firstY += direction[1]

                #If the field have at least one opponent's stone in its surroundings, append its coordinates to
                #self.candidates, break the loop for current empty field and move on another one
                if (
                        self.is_on_board(firstX, firstY)
                        and self.board[firstX][firstY] == self.opponent_color
                ):
                    self.candidates.append(i)
                    break

                #Else, continue incrementing next direcion
                else:
                    firstX = i[0]
                    firstY = i[1]
                    continue

    #Function which evaluates possible moves
    def possible_moves(self):

        #Iterating through the self.candidates array
        for i in self.candidates:
            status = False  #Represents whether at least one candidate could be valid move or not
            row = i[0]
            col = i[1]

            #Iterating through array of all directions
            for j in self.directions:
                current_row = row + j[0]
                current_col = col + j[1]
                first = True    #Represents whether the opponent's stone is within the range of 1 or more

                #If the field after first incrementation of the direction is on board and not equals to opponent color,
                #continue on next iteration
                if (
                        self.is_on_board(current_row, current_col) and
                        self.board[current_row][current_col] != self.opponent_color
                ):
                    continue

                #Until the field is on board and not equals to empty field, keep itearting through the board
                while self.is_on_board(current_row, current_col) and self.board[current_row][current_col] != self.empty:

                    #If the field is equal to opponent color, increment the direction value
                    if self.board[current_row][current_col] == self.opponent_color:
                        current_row += j[0]
                        current_col += j[1]
                        first = False

                    #If the filed is equal to my color, add its coordinates to array of results and break the while
                    elif self.board[current_row][current_col] == self.my_color:
                        if not first:
                            self.results.append(i)
                            status = True
                        break

                #If the validity of the filed is proved, break the loop and move on the next candidate
                if status:
                    break

    #Checking whether the cooridnates of the field are within the board
    def is_on_board(self, x_coord, y_coord):

        # If the coordinates are within <0, 7>, return True
        if (0 <= x_coord) and (x_coord <= 7) and (0 <= y_coord) and (y_coord <= 7):
            return True
        return False

    #Evaluating the best move based on Heuristic array
    def weighted_moves(self):
        best_move = 0
        highest_score = -100

        #Iterate through the whole array of results
        for move_pair in self.results:
            x = move_pair[0]
            y = move_pair[1]

            #Evaluating whether whether current filed has higher score than the best one
            if self.boardHeuristic[x][y] > highest_score:
                best_move = move_pair
                highest_score = self.boardHeuristic[x][y]

        return best_move    #Returning best move
