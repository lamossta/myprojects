#include "queue.h"

queue_t* create_queue(int capacity){
    queue_t *queue;
    
    queue = (queue_t*)malloc(sizeof(queue_t));
    queue->capacity = capacity;
    queue->counter = 0;
    queue->valArr = (void**)malloc(capacity * sizeof(void*));
    queue->first = 0;
    queue->last = -1;

    for (int i = 0; i < queue->capacity; i++){
        queue->valArr[i] = NULL;
    }

    return queue;

}

void delete_queue(queue_t *queue){
    free(queue->valArr);
    free(queue);
}


bool push_to_queue(queue_t *queue, void *data){
    bool retVal;
    
    if (queue->capacity > queue->counter){
        
        queue->last = (queue->last + 1) % queue->capacity;
        queue->valArr[queue->last] = data;
        queue->counter++;
        
        retVal = true; 
    }else{
        retVal = false;
    }
    
    return retVal;
}

void* pop_from_queue(queue_t *queue){
    void* returnVal;
    
    if (queue->counter == 0 ){
        returnVal = NULL;
    }else{
        returnVal = queue->valArr[queue->first];
        queue->valArr[queue->first] = NULL;
        queue->counter -= 1;
        queue->first = (queue->first + 1) % queue->capacity;
    }
    
    return returnVal;
}

void* get_from_queue(queue_t *queue, int idx){
    if (idx < 0 || idx > queue->counter){
        return NULL;
    }else{
        int index = (queue->first + idx) % queue->capacity;
        return queue->valArr[index];
    }
}

int get_queue_size(queue_t *queue){
    return queue->counter;
}



