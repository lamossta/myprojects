#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

/*Defining constants*/
#define MAX 1000000
#define MAX_PRIME_NUMBERS 78498 //Number of prime numbers from 2 to 10**6

/*Initializing functions*/
long long checkInput(void);

/*Sieve function takes pointer to the array of prime Numbers*/
void sieve(long* primeNumbers);

void primeDecomposer(long long, long* primeNumers);

/*Main function*/
int main(int argc, char *argv[]){
  long primeNumbers[MAX_PRIME_NUMBERS];
  long long number;
  int returnValue;
  
  /*Calling sieve function*/
  sieve(primeNumbers);

  /*If the return value of checkInput function is higher than 0, execute prime 
  decomposing of the given number*/
  while ((number = checkInput()) > 0){
    printf("Prvociselny rozklad cisla %lld je:\n", number);
     
    /*If the number is 1, print just 1 and progress on another number from 
    input*/
    if (number == 1){
      printf("1\n");
      continue;
    }
     
    /*If the number is not 1, execute primeDecomposer function to decompose 
    given number*/
    else{
      primeDecomposer(number, primeNumbers);
    }
     
    /*Assigning returnValue to zero if all conditons are passed successfully*/
    returnValue = 0;  
  }
   
  /*If variable number (equals to return value of checkInput function) 
  is lower than 0, required conditions are not met and the main function 
  returns 100 and send error message on error output*/
  if(number < 0){
    fprintf(stderr, "Error: Chybny vstup!\n");
    returnValue = 100;
  }
   
  return returnValue;
}
  
/*Function creates Erastothenes sieve*/
void sieve(long* primeNumbers){
  bool allNumbers[MAX]; //Intializing reference array

  /*Fill the array with trues*/
  for (long i = 2; i < MAX; i++){
    allNumbers[i] = true;
  }

  /*If the item on certain index is true, it is the prime number and all of 
  his multiples are not prime numbers*/
  for (long i = 2; i < MAX; i++){
    if (allNumbers[i] == true){
      
      /*Assining false value to non prime numbers 
      (multiples of prime numbers)*/
      for (long l = i * i ; l < MAX; l += i){
        allNumbers[l] = false;
      } 
    }
  }

  /*If the item from allNumbers array has true value, it is the prime number 
  and we add its index to separate array*/
  long j = 0;
  for (long i = 2; i < MAX; i++){
    if (allNumbers[i] == true){
      primeNumbers[j] = i;
      j++; 
    } 
  }
}

/*checkInput function checks whether the number from input passes all required 
conditions*/
long long checkInput(void){
  long long numberFunction;
 
  /*If scanf returns 1, input was correct and return number from input*/
  if((scanf("%lld", &numberFunction)) == 1){
    return numberFunction;
  }
   
  /*Else input was incorrect, return -1*/
  else{
    return -1;
  }
}

/*primeDecomposer function does prime decomposing*/
void primeDecomposer(long long numberP, long* primeNumbers){
  int counter;    //Describes how many times we divided by the same divider 
  
  //Incrementing 1 to round the sqrt
  long long iterationValue = sqrt(numberP) + 1; 
  
  bool firstDivider = true; //Indicates whether the number is first divider
   
  /*Iterate through all possible prime numbers array indexes
  (array created by sieve function) of the given number from 
  2 to (sqrt(givenNumber) + 1)*/
  for (long i = 0; i <= iterationValue; i++){
    counter = 0;

    /*Divide by current divider until the remainder is equal to zero*/
    while (numberP % primeNumbers[i] == 0 && numberP != 1){
      numberP = numberP / primeNumbers[i];
      counter += 1; 
    }

    /*Execute if the counter is higher than 0*/
    if (counter > 0){
      
       /*After each divider, print multiplication symbol*/
      if (firstDivider == false){
        printf(" x ");
      }
      
      /*If the counter is higher than 1, print the divider and its exponent*/
      if(counter > 1){
        printf("%ld^%d", primeNumbers[i], counter);
      }

      /*If the exponent of the the divider is 1, print just the divider*/
      if (counter == 1){
        printf("%ld", primeNumbers[i]);      
      }
      
      /*Set to 0 because first divider has already been printed*/
      firstDivider = false;
      continue; //Continue to next iteration
    }
    
    /*If the number is equal to 1 after the division, end prime decomposing*/
    if (numberP == 1){
      break;
    }
    
    /*If the number from input is prime number, print the number and break the
    for loop*/
    else if(i >= iterationValue && counter == 0 && numberP != 1){
         
      /*If the remainder is not the first divider, print multiplication 
      symbol before the number itself*/
      if(firstDivider == false){
        printf(" x ");
      }
         
      printf("%lld", numberP);
      break;
    }
    continue;
  }
  printf("\n");
}


