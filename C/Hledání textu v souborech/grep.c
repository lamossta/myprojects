//Code structure inspired by video on CW (https://youtu.be/AEHeNks2Dq4)

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


#define INIT_CAPACITY 1
#define RESIZE_CONSTANT 20

typedef struct{
  char* line;
  int size;
  int capacity;
}row_t;

bool findStr(row_t* myLine, char* pattern, int lenPattern);
int lengthPattern(char* str);
void closeFile(FILE **file);
void openFile(FILE **file, char* fileName);
void alloc_line(row_t* myLine);
void realloc_line(row_t* myLine);
void dealloc_line(row_t* myLine);
int scanLine(row_t* myLine, FILE *file);
void closeFile(FILE **f);

/*Main function*/
int main(int argc, char *argv[]){
  row_t line;
  FILE* f;
  int readUntil = 0;
  int returnVal;
  bool foundEval = false;

  openFile(&f, argv[2]);
  
  while (readUntil != 1){
    alloc_line(&line);
    readUntil = scanLine(&line, f);

    int length = lengthPattern(argv[1]);
    if (findStr(&line, argv[1], length) == true){
      printf("%s\n", line.line);
      foundEval = true;
    }
    
    dealloc_line(&line);
  }
  closeFile(&f);


  //foundEval = true;
  returnVal = foundEval == true ? 0 : 1; 
  
  return returnVal;
}

void alloc_line(row_t* myLine){
  myLine->line = (char*)malloc(sizeof(char));
  myLine->size = 0;
  myLine->capacity = INIT_CAPACITY;
  myLine->line[0] = '\0';
}

void realloc_line(row_t* myLine){
  myLine->capacity += RESIZE_CONSTANT;
  myLine->line = (char*) realloc(myLine->line, myLine->capacity * sizeof(char)); 
}


void dealloc_line(row_t* myLine){
  if (myLine->line != NULL){
    char* tmp = myLine->line;
    myLine->line = NULL;
    free(tmp);
  }
}

int scanLine(row_t* myLine, FILE *file){
  int status = 0;
  
  while(status == 0){
    char c = fgetc(file);

    if (myLine->size + 1 == myLine->capacity){
      realloc_line(myLine);
    }
    
    switch (c){
      case EOF:
        status = 1;
        break;
    
      case '\n':
        status = 2;
        break;      
      
      default:
        myLine->line[myLine->size++] = c;
        break;
    }
  }
  myLine->line[myLine->size++] = '\0';
  return status;
}

void openFile(FILE **f, char* fileName){
  *f = fopen(fileName, "r");
  if (*f == NULL){
    exit(EXIT_FAILURE);
  }
}

void closeFile(FILE **f){
  if (fclose(*f) == EOF) {
    exit(EXIT_FAILURE);
  }
}

int lengthPattern(char* str){
  if (str[0])
    return 1 + lengthPattern(str+1);
  else    
    return 0;
}

bool findStr(row_t* myLine, char* pattern, int lenPattern){
  bool findStat = false;
  int patternIndex = 0;
  int checkLen = 0;

  if (lenPattern == 0){
    findStat = true;
  }
  else{

    for (int i = 0; i < myLine->size; i++){

      if (checkLen == lenPattern){
        goto retEval;
      }
    
      if (checkLen < lenPattern && myLine->line[i] == pattern[patternIndex]){
        patternIndex += 1;
        checkLen += 1;
      }else{
        patternIndex = 0;
        checkLen = 0;
      }
    }
  }

  retEval:
    if (checkLen == lenPattern){
      findStat = true;
    }
    
  return findStat;
}

