/*Structure of the code inspired by the video from CW*/ 
/*https://youtu.be/13jjxz82XLM*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define ERR_CODE 100

/*Struct of all infos about every matrix*/
typedef struct matrix{
    int rows;
    int cols;
    int *valArr;
}matrix;

/*Declaring functions*/
matrix* allocateMatrix(int rows, int cols);
matrix* scanMatrix(void);
void dealocateMatrix(matrix **tmp);
void printMatrix(const matrix *const tmp);
char scanOp(void);
matrix* sumOp(matrix* matrix1P, matrix* matrix2P, int signP);
matrix* multiOp(matrix* matrix1P, matrix* matrix2P);

/*Main function*/
int main(int argc, char *argv[]){
    int returnVal = 0;
    int sign = 1;
    
    matrix* matrix1 = scanMatrix();
    char operation = scanOp();
    matrix* matrix2 = scanMatrix();
    matrix* resultMatrix = NULL;
    
    /*List of all possible operations used in the output*/
    switch (operation){
        case '+':
            resultMatrix = sumOp(matrix1, matrix2, sign);
            break;

        case '-':
            sign = -1;
            resultMatrix = sumOp(matrix1, matrix2, sign);
            break;

        case '*':
            resultMatrix = multiOp(matrix1, matrix2);
            break;

        default:
            fprintf(stderr, "Error: Chybny vstup!\n");
            exit(ERR_CODE);
            break;
    }

    /*If the allocation of resultMatrix failed, print error message*/
    /*Else print resultMatrix*/
    if (resultMatrix == NULL){
        returnVal = ERR_CODE;
        fprintf(stderr, "Error: Chybny vstup!\n");
    }else{
        printMatrix(resultMatrix);
    }
    
    /*Deallocating used matrixes*/
    dealocateMatrix(&matrix1);
    dealocateMatrix(&matrix2);
    dealocateMatrix(&resultMatrix);
    
    /*Returning exact value based on handling the output*/
    return returnVal;
}

/*Allocating matrice using malloc*/
matrix *allocateMatrix(int rowsP, int colsP){
    matrix *tmp = malloc(sizeof(matrix));
    
    /*If the allocation was successful, declare struct values for the matrix*/
    if(tmp){
        tmp->rows = rowsP;
        tmp->cols = colsP;
        tmp->valArr = malloc(sizeof(int) * rowsP * colsP);

        /*If the allocation failed, return NULL*/
        if(tmp->valArr == NULL){
            free(tmp);
            tmp = NULL;
        }

        /*Set all values of matrix to zero to avoid memory errors*/
        for (int i = 0; i < rowsP * colsP; i++){
            tmp->valArr[i] = 0;
        }
        
    }
    return tmp;
}

/*Scanning matrix from the input*/
matrix* scanMatrix(void){
    matrix *tmp = NULL;
    int rows, cols;
    bool errStatus = false;

    /*If the scan of matrix size and its allocation were successful, 
    start reading matrix values*/
    if (scanf("%d %d", &rows, &cols) == 2 && rows >= 0 && 
        cols >= 0 && (tmp = allocateMatrix(rows, cols))){
        int *tmpVals = tmp->valArr;
        
        /*Looping though all values of matrix from the input*/
        for (int i = 0; i < (rows * cols); i++){
            
            /*If scanning fails, deallocate matrix and go to errEval section*/
            if (scanf("%d", tmpVals++) != 1){
                dealocateMatrix(&tmp);
                errStatus = true;
                goto errEval;
            }
        }
    /*Else go to errEval section*/
    }else{
        errStatus = true;
        goto errEval;
    }

    /*Error evalution and handling printing error message and error code*/
    errEval:
        if (errStatus){
            fprintf(stderr, "Error: Chybny vstup!\n");
            exit(ERR_CODE);
        }
    return tmp; 
}

/*Deallocate initialised matrix*/
void dealocateMatrix(matrix **tmp){
    /*If the matrix values are not NULL, free the allocated matrix*/
    if (tmp && *tmp){
        if ((*tmp)->valArr){
            free((*tmp)->valArr);
        }
        free(*tmp);
        *tmp = NULL;
    }
}

/*Printing matrix*/
void printMatrix(const matrix *const tmp){

    /*If the matrix values are not null, print matrix and its size*/
    if (tmp && tmp->valArr){
        printf("%d %d\n", tmp->rows, tmp->cols);
        
        /*Looping through the whole matrix and printing values*/
        for (int i = 0; i < tmp->rows; i++){
            for (int j = 0; j < tmp->cols; j++){
                printf("%d", tmp->valArr[i * tmp->cols + j]);

                /*Spliting values with spaces*/
                if (j != (tmp->cols - 1)){
                    printf(" ");
                }
            }
            /*At the end of each row, print new line*/
            printf("\n");
        }
    }
}

/*Scanning opearation char*/
char scanOp(void){
    char op[2];
    
    /*If the scanning was successful, return the char*/
    /*Else return null terminating char*/
    if (scanf("%1s", op) != 1 ||
        !(op[0] == '-' || op[0] == '+' || op[0] == '*')){
        op[0] = '\0';
        return op[0];
    }
    return op[0];
}

/*Function to sum matrixes*/
matrix* sumOp(matrix* matrix1P, matrix* matrix2P, int signP){
    matrix* resultM = NULL;
    bool sameCols = matrix1P -> cols == matrix2P->cols;
    bool sameRows = matrix1P->rows == matrix2P->rows;
    
    /*If the matrices meet conditions for the sum, execute the sum*/
    /*Else print error message and exit the programme with error code*/
    if (sameCols && sameRows && 
        (resultM = allocateMatrix(matrix1P->rows, matrix2P->cols))){
        int *matrixValue1 = matrix1P->valArr;
        int *matrixValue2 = matrix2P->valArr;
        int *resultValue = resultM->valArr;
        
        /*Looping through the whole matrixes and executing sum operation*/
        for (int i = 0; i < (matrix1P->cols * matrix1P->rows); ++i){
            resultValue[i] = matrixValue1[i] + (signP * (matrixValue2[i]));    
        }
    }else{
        fprintf(stderr, "Error: Chybny vstup!\n");
        exit(ERR_CODE);
    }
    return resultM;
}

/*Function to execute multiplication of matrixes*/
matrix* multiOp(matrix* matrix1P, matrix* matrix2P){
    matrix* resultM = NULL;
    int tmpTot;
    bool multiCon = matrix1P->cols == matrix2P->rows;
    
    if (multiCon && (resultM = allocateMatrix(matrix1P->rows, matrix2P->cols))){
        
        int *resultValue = resultM->valArr;
        int *matrixValue1 = matrix1P->valArr;
        int *matrixValue2 = matrix2P->valArr;
        
        /*Looping through rows and cols of the first matrix and rows of the 
        second matrix and executing multiplication operation*/
        for (int i = 0; i < matrix1P->rows; i++){
            for (int j = 0; j < matrix2P->cols; j++){
                for (int k = 0; k < matrix2P->rows; k++){
                    tmpTot = matrixValue1[i * matrix1P->cols + k] * 
                    matrixValue2[k * matrix2P->cols + j];
                    
                    resultValue[i * resultM->cols + j] += tmpTot;
                    tmpTot = 0;
                }
            }
        } 
    /*Else print error message and end programme with error code*/
    }else{
        fprintf(stderr, "Error: Chybny vstup!\n");
        exit(ERR_CODE);
    }
    return resultM;
}

