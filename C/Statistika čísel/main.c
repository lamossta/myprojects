#include <stdio.h>
#include <stdlib.h>

/*Defining interval constants*/
#define INTERVALMIN -10000
#define INTERVALMAX 10000

/*Declaring functions*/
int checkPositivity(int inputNum);
int checkNegativity(int numberfromInput);

int main(int argc, char *argv[]){
  int inputNum;
  int inputValue;
  int inputCount = 0;
  static int negativeCount;
  static int positiveCount;
  int evenCount = 0;
  int oddCount = 0;
  int sumValue = 0;
  int maxNum = INTERVALMIN;  //Assigning max to -10000 for the first comparison
  int minNum = INTERVALMAX; //Assigning min to 10000 for the first comparison
  double positivePct; 
  double negativePct; 

  while ((inputValue = scanf("%d", &inputNum)) == 1) {
    /*Evaluating whether the number is within given interval*/
    if (inputNum < INTERVALMIN  || inputNum > INTERVALMAX){
      printf("\n");
      printf("Error: Vstup je mimo interval!\n");
      return 100;
    }
      
    /*Except first iteration, print comma and space in front of each number*/
    if (inputCount != 0){
      printf(", ");
    }

    /*Evaluating whether the number is even or odd*/
    if (inputNum % 2 == 0){
      evenCount += 1;
    }else{
      oddCount+= 1;
    }
    
    printf("%d", inputNum);
    inputCount += 1;
    negativeCount += checkNegativity(inputNum);
    positiveCount += checkPositivity(inputNum);
    sumValue += inputNum;
    
    maxNum = maxNum < inputNum ? inputNum: maxNum;  //Counting max number
    minNum = minNum > inputNum ? inputNum : minNum; //Counting min number 

    /*Counting percentage of positive and negative numbers*/
    positivePct = (double) positiveCount / inputCount * 100;
    negativePct = (double) negativeCount/inputCount * 100;
  }    
  
  /*Printing statistics of the numbers once scanf is equal to -1*/  
  if (inputValue == -1) {
    printf("\n");
    printf("Pocet cisel: %i\n", inputCount);
    printf("Pocet kladnych: %i\n", positiveCount); 
    printf("Pocet zapornych: %i\n", negativeCount); 
    printf("Procento kladnych: %.2lf\n", positivePct); 
    printf("Procento zapornych: %.2lf\n", negativePct); 
    printf("Pocet sudych: %i\n", evenCount);  
    printf("Pocet lichych: %i\n", oddCount); 
    printf("Procento sudych: %.2lf\n", (double) evenCount / inputCount * 100);  
    printf("Procento lichych: %.2lf\n", (double) oddCount / inputCount * 100); 
    printf("Prumer: %.2lf\n", (double) sumValue / inputCount); 
    printf("Maximum: %i\n", maxNum); 
    printf("Minimum: %i\n", minNum);
  
    return 0;
  }
}

/*Quantity of positive numbers from the input*/
int checkPositivity(int inputNum){ 
  if (inputNum > 0){
    return 1;
  }else{
    return 0;
  }
}

/*Quatity of negative numbers from the input*/
int checkNegativity(int inputNum){
  if (inputNum < 0){
    return 1;
  }else{
    return 0;
  }
}
