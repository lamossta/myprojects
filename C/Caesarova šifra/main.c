#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/*Defining constants*/
#define INITIAL_ARRAY_LENGTH 10
#define NUMBER_OF_ITERATIONS 52   //Number of all iterations in alphabet a-zA-Z 

/*Declaring functions*/
char *readInputMessage(int *msgLen);
void shiftAll(char *msgEncodedP, int msgEncodedLenP, char *msgP);
int compare(char compareArrP[], char *msgP, int msgEncodedLenP);
void printDecryptMsg(int offsetP, int msgEncodedLenP, char *msgEncodedP);

/*Main function*/
int main(int argc, char *argv[]){
  int returnVal = 0;
  
  /*Char pointers to arrays with message and encoded message*/
  char *msgEncoded, *msg;
  msgEncoded = msg = 0; 

  int msgEncodedLen , msgLen;
  msgEncodedLen = msgLen = 0;

  /*Filling msgEncoded with data from input*/
  msgEncoded = readInputMessage(&msgEncodedLen);

  /*Filling msg with input data when the previous allocation was successful*/
  if (msgEncoded){
    msg = readInputMessage(&msgLen);
  }

  /*Checking validity of input (equal length and allowed chars)*/
  if (msgEncoded == NULL || msg == NULL){
    fprintf(stderr, "Error: Chybny vstup!\n");
    returnVal = 100;
  }else if (msgLen != msgEncodedLen){
    fprintf(stderr, "Error: Chybna delka vstupu!\n");
    returnVal = 101;
  }

  /*If the input valid, execute my logic*/
  if (returnVal == 0){
   shiftAll(msgEncoded, msgEncodedLen, msg);
   printf("\n");
  }
  
  /*Freeing malloc arrays*/
  free(msgEncoded);
  free(msg);
  
  return returnVal;
}

/*Reading input message and allocating appropriate memory*/
char *readInputMessage(int *msgLen){
  int arrayCapacity = INITIAL_ARRAY_LENGTH;
  char *ret = malloc(arrayCapacity);
  
  int inputCh;
  int len = 0;

  /*Getting data from input*/
  while ((inputCh = getchar()) != EOF && inputCh != '\n'){
    /*Checking whether the input only has allowed chars*/
    if (!((inputCh >= 'a' && inputCh <= 'z') || 
       (inputCh >= 'A' && inputCh <= 'Z'))){
      
      free(ret);
      len = 0;
      ret = NULL;
      break;
    }

    /*Reallocating array capacity if the input is longer than our constant*/
    if (len == arrayCapacity){
      char *tmp = realloc(ret, 2 *arrayCapacity);
      arrayCapacity *= 2;
      ret = tmp;
      /*Checking whether the reallocation was successful*/
      if (tmp == NULL){
        free(ret);
        ret = NULL;
        len = 0;
        break;
      }
    }
    ret[len] = inputCh;
    len++;
  }
  
  *msgLen = len;
  return ret;
}

/*Shifting input message*/
void shiftAll(char *msgEncodedP, int msgEncodedLenP, char *msgP){
  char *compareArr =  malloc(msgEncodedLenP);
  int offset = 0;
  int sameChars = 0;

  /*New array filled with chars from encoded message*/
  for (int i = 0; i < msgEncodedLenP; i++){
    compareArr[i] = msgEncodedP[i];
  }
  
  /*Looping through all possible offsets*/
  for (int i = 0; i < NUMBER_OF_ITERATIONS; i++){
    int tmpSameChars = compare(compareArr, msgP, msgEncodedLenP);
      
    /*Evaluating offset*/
    if (tmpSameChars > sameChars){
      offset = i;
      sameChars = tmpSameChars;
    }
  
    /*Looping through all chars of encoded message*/
    for (int j = 0; j < msgEncodedLenP; j++){
      compareArr[j] += 1;

      /*Transfer from a-z to A-Z alphabet*/
      if (compareArr[j] == 123){
        compareArr[j] = 'A';
      }

      /*Transfer from A-Z to a-z alphabet*/
      else if (compareArr[j] == 91){
        compareArr[j] = 'a';
      }
    }
  }
  printDecryptMsg(offset, msgEncodedLenP, msgEncodedP);
  free(compareArr);
}

/*Comparing shifted message with message from input*/
int compare(char *compareArrP, char *msgP, int msgEncodedLenP){
  int matchingChars = 0;

  /*Looping through all chars of message from input and our shifted message*/
  for (int i = 0; i < msgEncodedLenP; i++){
    
    /*Checking whether the chars are the same*/
    if (msgP[i] == compareArrP[i]){
      matchingChars += 1;
    }
  }
  /*Returning number of matching chars*/
  return matchingChars;
}

/*Shifting message by found offset and printing the result*/
void printDecryptMsg(int offsetP, int msgEncodedLenP, char *msgEncodedP){
  /*Looping by offset value*/
  for (int i = 0; i < offsetP; i++){
    /*Looping through all chars from message encoded array*/
    for (int j = 0; j < msgEncodedLenP; j++){
      
      msgEncodedP[j] += 1;

      /*Transfer from a-z to A-Z alphabet*/
      if (msgEncodedP[j] == 123){
        msgEncodedP[j] = 'A';
      }

      /*Transfer from A-Z to a-z alphabet*/
      else if (msgEncodedP[j] == 91){
        msgEncodedP[j] = 'a';
      }
    }
  }

  /*Printing the result*/
  for (int i = 0; i < msgEncodedLenP; i++){
    printf("%c", msgEncodedP[i]);
  }
}




